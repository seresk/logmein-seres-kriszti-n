$(function() {
    $( "#dob" ).datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:+0"
    });
    $( "#dob" ).datepicker("option", "dateFormat","yy-mm-dd");

    $(document).ready(function () {

        $('#checkForm').submit(function (e) {
            var data = {
                name: $('input[name=name]').val(),
                dob: $('input[name=dob]').val(),
                email: $('input[name=email]').val(),
                occupation: $('input[name=occupation]').val()
            };
            var user = {data: JSON.stringify(data)};
            $.ajax({
                type: 'POST',
                url: 'http://localhost:3000/api',
                data: user
            })
                .done(function (e) {
                    $("#result").html(e);
                });

            e.preventDefault();
        });
    });

    function meetsMinimumAge(birthDate, minAge) {
        /*
         * http://stackoverflow.com/questions/9389789/javascript-older-than-18-on-leap-years
         * */

        var tempDate = new Date(birthDate.getFullYear() + minAge, birthDate.getMonth(), birthDate.getDate());
        return (tempDate <= new Date());
    }

    $("#dob").focusout(function(){

        if($("input[name=dob]").val().length > 0){
            if(new Date($("input[name=dob]").val()) ==  "Invalid Date"){ // this would do: http://formvalidation.io/validators/date/
                $("#result").html("Invalid Date");
                setTimeout(function(){$("#result").html(" ");},5000);


            }else{
                if(!meetsMinimumAge(new Date($("input[name=dob]").val()),18)){
                    $("#result").html("Must be 18 or older.");
                    setTimeout(function(){$("#result").html(" ");},5000);
                }else
                    $("#result").html("");
            }
        }
    })

});