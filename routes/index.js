var express = require('express');
var router = express.Router();


function meetsMinimumAge(birthDate, minAge) {
  /*
   * http://stackoverflow.com/questions/9389789/javascript-older-than-18-on-leap-years
   * */

  var tempDate = new Date(birthDate.getFullYear() + minAge, birthDate.getMonth(), birthDate.getDate());
  return (tempDate <= new Date());
}

function validateEmail(email) {
  /**
   *
   * http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
   */
  var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  return re.test(email);
}


/* GET/Post home page. */
router.all('/', function(req, res, next) {
  //if(typeof req !== "undefined")
  var passed ="";
  if(req.method == "POST"){ //form will never be submitted, I chose AJAX instead.

    if( meetsMinimumAge(new Date(req.body.dob), 18) ){
      console.log("Age Passed");
      passed = passed +" Age OK."
    }
    else{
      console.log("Nop");
      passed = " Age failed."
    }
    if( validateEmail(req.body.email) ){
      console.log("Passed");
      passed = passed +"Email OK."
    }
    else{
      console.log("Nop");
      passed = passed+ " Email failed."
    }

    if(req.body.name.lenght>0 ){
      console.log("Passed");
      passed = passed +"Name OK."
    }
    else{
      console.log("Nop");
      passed = passed+ " Name failed."
    }
  //console.log(req.body);
  }



  res.render('index', { passed: passed });

});

router.post('/api', function (req, res) {
  data = JSON.parse(req.body.data);

  var passed ="";
  if(typeof data.dob !== "undefined" && meetsMinimumAge(new Date(data.dob), 18) ){
    console.log("Age Passed");
    passed = passed +" Age OK."
  }
  else{
    console.log("Nop");
    passed = " Age failed."
  }

  if( typeof data.email !== "undefined" && validateEmail(data.email) ){
    console.log("Passed");
    passed = passed +"Email OK."
  }
  else{
    console.log("Nop");
    passed = passed+ " Email failed."
  }

  if(typeof data.name !== "undefined" &&  data.name.length > 0 ){
    console.log("Passed");
    passed = passed +"Name OK."
  }
  else{
    console.log("Nop");
    passed = passed+ " Name failed."
  }

  res.send(passed);
});


module.exports = router;
